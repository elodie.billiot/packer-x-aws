
variable "ami_name" {
  type    = string
  default = "${env("ami_name")}"
}

variable "aws_target_account" {
  type    = string
  default = "${env("aws_target_account")}"
}

variable "user_id" {
  type    = string
  default = "${env("user_id")}"
}

variable "region" {
  type    = string
  default = "${env("AWS_REGION")}"
}

data "amazon-ami" "master" {
  filters = {
    name = "CIS Amazon Linux 2 Benchmark v2.0.0.* - Level 1-4c096026-c6b0-440c-bd2f-6d34904e4fc6"
  }
  max_retries = "2"
  most_recent = true
  owners      = ["679593333241"]
  region      = var.region
}

source "amazon-ebs" "master" {
  ami_name                    = "${var.ami_name}"
  ami_users                   = ["${var.user_id}"]
  associate_public_ip_address = "true"
  instance_type               = "t3.medium"
  launch_block_device_mappings {
    delete_on_termination = true
    device_name           = "/dev/xvda"
    encrypted             = true
    kms_key_id            = "alias/${var.aws_target_account}"
    volume_size           = 100
  }
  max_retries  = "2"
  source_ami   = "${data.amazon-ami.master.id}"
  ssh_username = "ec2-user"
}

build {
  sources = ["source.amazon-ebs.master"]

  provisioner "ansible" {
    ansible_env_vars = [ "AWS_REGION=${var.region}" ]
    extra_arguments = ["--extra-vars", "aws_target_account=${var.aws_target_account}", "--extra-vars", "ansible_python_interpreter=/usr/bin/python3"]
    playbook_file   = "ansible/master.yml"
  }
}
